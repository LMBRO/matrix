/*
 * Matrix.h
 *
 *  Created on: 2017-02-09
 *      Author: etudiant
 */

#ifndef MATRIX_H_
#define MATRIX_H_

#include <vector>

template <typename Object>
class Matrix{
public:
	Matrix();
	Matrix(unsigned int,unsigned int);
	Matrix(std::vector<std::vector<Object>> const &); //modified, was passed by value but there would have been two copy constructions instead of one
	Matrix(std::vector<std::vector<Object>> &&);
	std::vector<Object> const & operator[](unsigned int) const;
	std::vector<Object> & operator[](unsigned int);
	unsigned int numrows() const;
	unsigned int numcols() const;
	void resize(unsigned int, unsigned int);
private:
	std::vector<std::vector<Object>> array;
};

#include "Matrix.hpp"

#endif /* MATRIX_H_ */
