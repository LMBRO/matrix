/*
 * Matrix.hpp
 *
 *  Created on: 2017-02-09
 *      Author: etudiant
 */

template<typename Object>
Matrix<Object>::Matrix() {
}

template<typename Object>
Matrix<Object>::Matrix(unsigned int rows, unsigned int cols) :
		array { rows } {
	for (auto & thisRow : array) {
		thisRow.resize(cols);
	}
}

template<typename Object>
Matrix<Object>::Matrix(const std::vector<std::vector<Object> >& v) :
		array { v } {
}

template<typename Object>
Matrix<Object>::Matrix(std::vector<std::vector<Object> >&& v) :
		array { std::move(v) } {
}

template<typename Object>
const std::vector<Object>& Matrix<Object>::operator [](unsigned int row) const {
	return array[row];
}

template<typename Object>
std::vector<Object>& Matrix<Object>::operator [](unsigned int row) {
	return array[row];
}

template<typename Object>
unsigned int Matrix<Object>::numrows() const {
	return array.size();
}

template<typename Object>
unsigned int Matrix<Object>::numcols() const {
	if (numrows() == 0)
		return 0;
	else
		return array[0].size();
}
template<typename Object>
void Matrix<Object>::resize(unsigned int newrows, unsigned int newcols) {
	// deleting rows => loss of data
	array.resize(newrows);
	for (auto & thisrow: array){
		thisrow.resize(newcols);
	}
}
